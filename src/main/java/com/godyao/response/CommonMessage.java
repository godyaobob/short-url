package com.godyao.response;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author godyao
 * @date 2022/3/5
 */
@Data
public class CommonMessage<T> implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 作用： 状态码，有0，1，500 <br>
     * 作者：suntianchi<br>
     * 创建日期： 2018/12/24<br>
     * 版本： V1.0<br>
     */
    private Integer statusCode;
    /**
     * 作用：  message，返回状态信息<br>
     * 作者：suntianchi<br>
     * 创建日期： 2018/12/24<br>
     * 版本： V1.0<br>
     */
    private String msg;
    /**
     * 方法描述：返回数据<br>
     * 作者： Sun Tianchi <br>
     * 创建日期： 2018/9/11 <br>
     * 版本： V1.0<br>
     */
    private T data;

    private Map meta;

    public CommonMessage() {

    }


    public CommonMessage(T data) {
        this.statusCode = 0;
        this.data = data;
        if (data == null) {
            this.data = (T) new ArrayList<>();
        }
        System.out.println("===============>" + this.data);
        this.msg = "SUCCESS";
    }

    public CommonMessage(T data, Map meta) {
        this.statusCode = 0;
        this.data = data;
        if (data == null) {
            this.data = (T) new ArrayList<>();
        }
        this.msg = "SUCCESS";
        this.meta = meta;
    }

    public CommonMessage(T data, Map meta,int statusCode) {
        this.statusCode = 0;
        this.data = data;
        this.msg = "SUCCESS";
        this.meta = meta;
    }

    public CommonMessage(Integer code, String message) {
        this.statusCode = code;
        this.msg = message;
    }

    public static CommonMessage newInstance(Object data){
        return new CommonMessage(data, null,0);
    }

}