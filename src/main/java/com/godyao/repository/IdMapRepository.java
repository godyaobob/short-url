package com.godyao.repository;

import com.godyao.entity.IdMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IdMapRepository extends JpaRepository<IdMap, Long> {
    Optional<IdMap> findByCustomerId(Long customerId);

}