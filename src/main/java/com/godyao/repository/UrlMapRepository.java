package com.godyao.repository;

import com.godyao.entity.UrlMap;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UrlMapRepository extends CrudRepository<UrlMap, Long> {
    @Query(nativeQuery = true, value = "select u.long_url from url_map u where u.id = ?1 and u.expire_time > now()")
    String findActiveLongUrlByShortUrl(long id);

    @Query(nativeQuery = true, value = "select u.id from url_map u where u.long_url = ?1")
    Long findIdByLongUrl(String longUrl);


    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from url_map u where u.expire_time < now()")
    int deleteExpiredData();


}