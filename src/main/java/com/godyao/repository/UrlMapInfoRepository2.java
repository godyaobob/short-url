//package com.godyao.repository;
//
//import com.godyao.entity.UrlMapInfoTest;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Lock;
//import org.springframework.data.jpa.repository.Query;
//
//import javax.persistence.LockModeType;
//
//
//public interface UrlMapInfoRepository2 extends JpaRepository<UrlMapInfoTest, Long> {
//    //@Lock(LockModeType.PESSIMISTIC_WRITE)
//    UrlMapInfoTest findByLongUrl(String longUrl);
//
//
//    @Query(nativeQuery = true, value = "select u.long_url from url_map_info2 u where u.id = ?1 and u.expire_time > now()")
//    String findActiveLongUrlByShortUrl(Long id);
//
//
//
//}