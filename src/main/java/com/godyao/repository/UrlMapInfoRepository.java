//package com.godyao.repository;
//
//import com.godyao.entity.UrlMapInfo;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Lock;
//import org.springframework.data.jpa.repository.Query;
//
//import javax.persistence.LockModeType;
//
//public interface UrlMapInfoRepository extends JpaRepository<UrlMapInfo, Long> {
//    @Query(nativeQuery = true, value = "select u.long_url from url_map_info u where u.short_url = ?1 and u.expire_time > now()")
//    String findActiveLongUrlByShortUrl(String shortUrl);
//
//
//    @Query(nativeQuery = true, value = "select max(id) from url_map_info")
//    Long findMaxId();
//
//
//    //@Query("select longUrl from UrlMapInfo where shortUrl")
//    //String findMaxId(String short);
//}