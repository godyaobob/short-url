package com.godyao.utils;

/**
 * @author godyao
 * @date 2022/3/8
 */
public class Base62Util {
    private static String BASE62 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static String encode(long id) {
        StringBuilder stringBuilder = new StringBuilder();
        while (id > 0) {
            stringBuilder.append(BASE62.charAt((int) (id % 62)));
            id = id / 62;
        }
        while (stringBuilder.length() < 6) {
            stringBuilder.append(0);
        }
        return stringBuilder.reverse().toString();
    }

    public static long decode(String shortKey) {
        long id = 0;
        for (int i = 0; i < shortKey.length(); i++) {
            id = id * 62 + BASE62.indexOf(shortKey.charAt(i));
        }
        return id;
    }

    public static void main(String[] args) {
        System.out.println(Long.MAX_VALUE);
        System.out.println(encode(Long.valueOf("5864128586074112")));
        System.out.println(encode(Long.MAX_VALUE));
    }

}
