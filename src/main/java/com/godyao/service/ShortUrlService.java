package com.godyao.service;

import com.godyao.entity.UrlMap;
import com.godyao.request.CommonRequest;


/**
 * @author godyao
 * @date 2022/3/5
 */
public interface ShortUrlService {
    String shorten(UrlMap urlMap) throws Exception;

    String redirect(UrlMap urlMap);

    String findShortByLong(UrlMap urlMap);

    void customerUrl(CommonRequest commonRequest) throws Exception;
}
