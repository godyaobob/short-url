//package com.godyao.service.impl;
//
//import cn.hutool.core.date.DateUtil;
//import com.godyao.entity.UrlMapInfoTest;
//import com.godyao.repository.UrlMapInfoRepository2;
//import com.godyao.service.ShortUrlService;
//import com.godyao.utils.Base62Util;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Primary;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//import java.util.Optional;
//
///**
// * @author godyao
// * @date 2022/3/5
// */
////@Primary
//@Service
//public class ShortUrlServiceImpl2 implements ShortUrlService {
//    private static final int DEFAULT_EXPIRE_DAYS = 3;
//
//    private static final String DEFAULT_DIRECT_URL = "http://www.baidu.com";
//
//    private static final String DOMAIN = "http://micro/";
//
//
//    @Autowired
//    private UrlMapInfoRepository2 urlMapInfoRepository;
//
//    @Override
//    public String shorten(String longUrl, Date expireTime) {
//        if (null == expireTime) {
//            expireTime = DateUtil.offsetDay(DateUtil.date(), DEFAULT_EXPIRE_DAYS);
//        }
//        final UrlMapInfoTest byLongUrl = urlMapInfoRepository.findByLongUrl(longUrl);
//        if (null != byLongUrl) {
//            return DOMAIN + Base62Util.encode(byLongUrl.getId());
//        }
//        final UrlMapInfoTest save = urlMapInfoRepository.save(UrlMapInfoTest.builder().longUrl(longUrl).expireTime(expireTime).build());
//
//        return DOMAIN + Base62Util.encode(save.getId());
//    }
//
//    @Override
//    public String redirect(String shortUrl) {
//        final long decode = Base62Util.decode(shortUrl);
//        return Optional.ofNullable(urlMapInfoRepository.findActiveLongUrlByShortUrl(decode)).orElse(DEFAULT_DIRECT_URL);
//    }
//}
