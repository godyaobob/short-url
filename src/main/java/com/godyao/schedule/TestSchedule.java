package com.godyao.schedule;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author godyao
 * @date 2022/3/9
 */
@Slf4j
//@Service
public class TestSchedule {

    @Scheduled(cron = "* * * * * ?")
    @Async
    public void task1() {
        log.info("task1 == > 开始执行");
        try {
            TimeUnit.MINUTES.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("task1 == > 执行结束");
    }

    @Scheduled(cron = "* * * * * ?")
    public void task2() {
        log.info("task2 == > 开始执行");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("task2 == > 执行结束");
    }
}
