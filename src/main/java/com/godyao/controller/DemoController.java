package com.godyao.controller;

import cn.hutool.core.date.DateUtil;
import com.godyao.entity.UrlMap;
import com.godyao.request.CommonRequest;
import com.godyao.response.CommonMessage;
import com.godyao.service.ShortUrlService;
import com.godyao.utils.Base62Util;
import com.godyao.validated.groups.Group1;
import com.godyao.validated.groups.Group2;
import com.godyao.validated.groups.Group3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author godyao
 * @date 2022/3/5
 */
@RestController
public class DemoController {
    @Autowired
    private ShortUrlService shortUrlService;

    @PostMapping("/shorten")
    public CommonMessage shorten(@Validated(value = Group1.class) @RequestBody CommonRequest commonRequest) throws Exception {
        //new Thread(() -> {
        //    for (int i = 0; i < 10000; i++) {
        //        String s = "http://longLinkdomain/" + i;
        //        String shortUrl = null;
        //        try {
        //            shortUrl = shortUrlService.shorten(UrlMap.builder().longUrl(s).expireTime(commonRequest.getExpireTIme()).build());
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        //        System.out.println("生成短链接：" + shortUrl + ", 获取到原始链接为：" + s);
        //    }
        //}).start();
        //
        //new Thread(() -> {
        //    for (int i = 10000; i < 20000; i++) {
        //        String s = "http://longLinkdomain/" + i;
        //        String shortUrl = null;
        //        try {
        //            shortUrl = shortUrlService.shorten(UrlMap.builder().longUrl(s).expireTime(commonRequest.getExpireTIme()).build());
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        //        System.out.println("生成短链接：" + shortUrl + ", 获取到原始链接为：" + s);
        //    }
        //}).start();
        //
        //new Thread(() -> {
        //    for (int i = 20000; i < 30000; i++) {
        //        String s = "http://longLinkdomain/" + i;
        //        String shortUrl = null;
        //        try {
        //            shortUrl = shortUrlService.shorten(UrlMap.builder().longUrl(s).expireTime(commonRequest.getExpireTIme()).build());
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        //        System.out.println("生成短链接：" + shortUrl + ", 获取到原始链接为：" + s);
        //    }
        //}).start();
        //
        //new Thread(() -> {
        //    for (int i = 30000; i < 40000; i++) {
        //        String s = "http://longLinkdomain/" + i;
        //        String shortUrl = null;
        //        try {
        //            shortUrl = shortUrlService.shorten(UrlMap.builder().longUrl(s).expireTime(commonRequest.getExpireTIme()).build());
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        //        System.out.println("生成短链接：" + shortUrl + ", 获取到原始链接为：" + s);
        //    }
        //}).start();
        //
        //new Thread(() -> {
        //    for (int i = 40000; i < 50000; i++) {
        //        String s = "http://longLinkdomain/" + i;
        //        String shortUrl = null;
        //        try {
        //            shortUrl = shortUrlService.shorten(UrlMap.builder().longUrl(s).expireTime(commonRequest.getExpireTIme()).build());
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        //        System.out.println("生成短链接：" + shortUrl + ", 获取到原始链接为：" + s);
        //    }
        //}).start();
        //
        //new Thread(() -> {
        //    for (int i = 50000; i < 60000; i++) {
        //        String s = "http://longLinkdomain/" + i;
        //        String shortUrl = null;
        //        try {
        //            shortUrl = shortUrlService.shorten(UrlMap.builder().longUrl(s).expireTime(commonRequest.getExpireTIme()).build());
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        //        System.out.println("生成短链接：" + shortUrl + ", 获取到原始链接为：" + s);
        //    }
        //}).start();


        //for (int i = 0; i < 100000000; i++) {
        //    String s = "http://longLinkDomain/" + i;
        //    String shortUrl = shortUrlService.shorten(UrlMap.builder().longUrl(s).expireTime(commonRequest.getExpireTIme()).build());
        //    System.out.println("生成短链接：" + shortUrl + ", 获取到原始链接为：" + s);
        //}
        //return "";
        return new CommonMessage(shortUrlService.shorten(UrlMap.builder().longUrl(commonRequest.getLongUrl()).expireTime(commonRequest.getExpireTime()).build()));
    }

    @GetMapping("/redirect")
    public void redirect(@Validated(value = Group2.class) CommonRequest commonRequest, HttpServletResponse response) throws IOException {
        final String shortUrl = commonRequest.getShortUrl();
        final long id = Base62Util.decode(shortUrl);
        //final UrlMap urlMap = new UrlMap();
        //urlMap.setId(id);
        final String redirect = shortUrlService.redirect(UrlMap.builder().id(id).build());
        //final String redirect = shortUrlService.redirect(urlMap);
        System.out.println(redirect);
        response.sendRedirect(redirect);
    }

    @GetMapping("/findShortByLong")
    public String findShortByLong(@Validated(value = Group1.class) CommonRequest commonRequest) {
        final String longUrl = commonRequest.getLongUrl();
        final String shortUrl = shortUrlService.findShortByLong(UrlMap.builder().longUrl(longUrl).build());
        return shortUrl;
    }

    @PostMapping("/customerUrl")
    public CommonMessage customerUrl(@Validated(value = Group3.class) @RequestBody CommonRequest commonRequest) throws Exception {
        if (commonRequest.getExpireTime() != null) {
            final int compare = DateUtil.compare(commonRequest.getExpireTime(), DateUtil.date());
            if (compare < 0) {
                throw new Exception("expireTime必须是未来时间");
            }
        }
        shortUrlService.customerUrl(commonRequest);
        return new CommonMessage(0, "OK");
    }
}
