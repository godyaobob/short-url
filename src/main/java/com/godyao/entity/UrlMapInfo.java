//package com.godyao.entity;
//
//import lombok.*;
//
//import javax.persistence.*;
//import java.util.Date;
//
///**
// * @author godyao
// * @date 2022/3/5
// */
//@Entity
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Table(name = "url_map_info")
//public class UrlMapInfo {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", nullable = false)
//    private Long id;
//
//    @Column(name = "long_url", unique = true, nullable = false)
//    private String longUrl;
//
//    @Column(name = "short_url", unique = true, nullable = false)
//    private String shortUrl;
//
//    /**
//     * 创建时间
//     */
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date createTime;
//
//    /**
//     * 过期时间
//     */
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date expireTime;
//
//
//}
