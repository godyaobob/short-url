package com.godyao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @author godyao
 * @date 2022/3/5
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "url_map")
public class UrlMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "long_url", unique = true, nullable = false)
    private String longUrl;

    /**
     * 过期时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date expireTime;


}
