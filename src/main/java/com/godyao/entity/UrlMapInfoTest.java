//package com.godyao.entity;
//
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//import java.util.Date;
//
///**
// * @author godyao
// * @date 2022/3/5
// */
//@Entity
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Table(name = "url_map_info2")
//public class UrlMapInfoTest {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", nullable = false)
//    private Long id;
//
//    @Column(name = "long_url", unique = true, nullable = false,
//    columnDefinition = "VARBINARY(200)")
//    private String longUrl;
//
//
//    /**
//     * 过期时间
//     */
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date expireTime;
//
//
//}
