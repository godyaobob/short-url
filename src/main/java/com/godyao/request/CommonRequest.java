package com.godyao.request;

import com.godyao.validated.groups.Group1;
import com.godyao.validated.groups.Group2;
import com.godyao.validated.groups.Group3;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.Date;

/**
 * @author godyao
 * @date 2022/3/5
 */
@Validated
@Data
public class CommonRequest {
    @NotBlank(message = "不能为空", groups = {Group1.class, Group3.class})
    private String longUrl;
    @NotBlank(message = "不能为空", groups = {Group2.class, Group3.class})
    private String shortUrl;
    @Future(message = "必须是未来时间", groups = Group1.class)
    private Date expireTime;
}
