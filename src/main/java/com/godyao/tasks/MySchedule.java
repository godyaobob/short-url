package com.godyao.tasks;

import com.godyao.repository.UrlMapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

/**
 * @author godyao
 * @date 2022/3/12
 */
@Component
public class MySchedule {
    @Autowired
    private UrlMapRepository urlMapRepository;

    @Scheduled(cron = "0 0 1 * * ?")
    @PostConstruct
    public void deleteExpiredData() {
        urlMapRepository.deleteExpiredData();
    }
}
