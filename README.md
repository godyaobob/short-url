# short-url

#### 介绍
短链接系统
URL缩短用于为长URL创建较短的别名。我们称这些缩短的别名为 **“短链接”** 。当用户点击这些短链接时，会重定向到原始URL。显示、打印、发送消息或推特时，短链接可节省大量空间。此外，用户不太可能错误键入较短的URL。


#### 软件架构
软件架构说明
本文主要使用shardingSphere-jdbc做分库分表.使用其自带的SNOWFLAKE生产主键来与长链接绑定入库.在使用Base62算法将ID生产一个字符串然后跟域名拼接作为短链接.


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
